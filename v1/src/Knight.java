public class Knight extends Fighter {

	public Knight(int baseHp, int wp) {
		super(baseHp, wp);
	}
	
	public double getCombatScore() {
		if(Utility.isSquare(Battle.GROUND)==false) {
			if(getWp()==1) return getBaseHp();
			else return (double)getBaseHp()/10;
		}
		else return (double)getBaseHp()*2;
	}
}
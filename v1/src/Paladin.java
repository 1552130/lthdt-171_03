
public class Paladin extends Knight {

	public Paladin(int baseHp, int wp) {
		super(baseHp, wp);
	}
	public double getCombatScore() {
		int N=0;
		int fibo1=0;
		int fibo2=1;
		int fibo=0;
		while(fibo1 < getBaseHp()) {
			fibo=fibo1+fibo2;
			fibo1=fibo2;
			fibo2=fibo;
			N++;
		}
		if (fibo1 == getBaseHp() && N>2) return 1000 + N;
		else return (double)3*getBaseHp();
	}
}
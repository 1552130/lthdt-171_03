public class Warrior extends Fighter {

	public Warrior(int baseHp, int wp) {
		super(baseHp, wp);
	}

	public double getCombatScore() {
		if(Utility.isPrime(Battle.GROUND)==false) {
			if(getWp()==1) return getBaseHp();
			else return (double)getBaseHp()/10;
		}
		else return (double)getBaseHp()*2;
	}    
}